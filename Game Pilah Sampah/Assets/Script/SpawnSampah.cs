﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnSampah : MonoBehaviour
{
    public float pause = 0.8f;
    float timer;
    public GameObject[] objectSampah;


    // Spawn a random Organic / Non-Organic every pause
    void Update()
    {
        timer += Time.deltaTime;
        if(timer > pause)
        {
            int random = Random.Range(0, objectSampah.Length);
            Instantiate(objectSampah[random], transform.position, transform.rotation);
            timer = 0;
        }
    }
}
