﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DetectSampah : MonoBehaviour
{
    public string nameTag;
    public AudioClip audioTrue;
    public AudioClip audioFalse;
    private AudioSource mediaPlayerTrue;
    private AudioSource mediaPlayerFalse;
    public Text textScore;


    // Initialize Audio to Object
    void Start()
    {
        mediaPlayerTrue = gameObject.AddComponent<AudioSource>();
        mediaPlayerTrue.clip = audioTrue;

        mediaPlayerFalse = gameObject.AddComponent<AudioSource>();
        mediaPlayerFalse.clip = audioFalse;
    }

    // Check if Tag of object is correct
    void OnTriggerEnter2D (Collider2D collision)
    {
        if (collision.tag.Equals(nameTag))
        {
            Data.score += 10;
            textScore.text = Data.score.ToString();
            Destroy(collision.gameObject);
            mediaPlayerTrue.Play();
        }
        else
        {
            Data.score -= 15;
            textScore.text = Data.score.ToString();
            Destroy(collision.gameObject);
            mediaPlayerFalse.Play();
        }
    }

    // Check for Game Over Condition
    void Update()
    {
        if(Data.score < 0)
        {
            SceneManager.LoadScene("GameOver");
        }
    }
}
